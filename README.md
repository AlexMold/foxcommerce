# FoxCommerce 



## Installation

```sh
npm run build
```

## Installation for dev

```sh
npm run start
npm run server
```



## Dependencies

- [babel-polyfill](): Provides polyfills necessary for a full ES2015+ environment
- [express](): Fast, unopinionated, minimalist web framework
- [nodemon](https://github.com/remy/nodemon): Simple monitor script for use during development of a node.js app.
- [react](): React is a JavaScript library for building user interfaces.
- [react-dom](): React package for working with the DOM.
- [react-redux](https://github.com/reactjs/react-redux): Official React bindings for Redux
- [redux](https://github.com/reactjs/redux): Predictable state container for JavaScript apps
- [semantic-ui-react](https://github.com/Semantic-Org/Semantic-UI-React): The official Semantic-UI-React integration.
- [webpack](https://github.com/webpack/webpack): Packs CommonJs/AMD modules for the browser. Allows to split your codebase into multiple bundles, which can be loaded on demand. Support loaders to preprocess files, i.e. json, jade, coffee, css, less, ... and your custom stuff.
- [yarn](): 

## Dev Dependencies

- [autoprefixer](): Parse CSS and add vendor prefixes to CSS rules using values from the Can I Use website
- [autoprefixer-stylus](https://github.com/jescalan/autoprefixer-stylus): autoprefixer for stylus
- [babel-core](): Babel compiler core.
- [babel-eslint](https://github.com/babel/babel-eslint): Custom parser for ESLint
- [babel-loader](https://github.com/babel/babel-loader): babel module loader for webpack
- [babel-plugin-transform-object-rest-spread](): Compile object rest and spread to ES5
- [babel-preset-es2015](): Babel preset for all es2015 plugins.
- [babel-preset-react](): Babel preset for all React plugins.
- [bitbar-webpack-progress-plugin](): 
- [css-loader](https://github.com/webpack/css-loader): css loader module for webpack
- [eslint](): An AST-based pattern checker for JavaScript.
- [eslint-config-airbnb](https://github.com/airbnb/javascript): Airbnb&#39;s ESLint config, following our styleguide
- [eslint-config-airbnb-base](https://github.com/airbnb/javascript): Airbnb&#39;s base JS ESLint config, following our styleguide
- [eslint-loader](): eslint loader (for webpack)
- [eslint-plugin-import](https://github.com/benmosher/eslint-plugin-import): Import with sanity.
- [eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react): React specific linting rules for ESLint
- [image-webpack-loader](https://github.com/tcoopman/image-webpack-loader): Image loader module for webpack
- [imagemin-jpegtran](): jpegtran imagemin plugin
- [imagemin-pngquant](): pngquant imagemin plugin
- [node-sass](https://github.com/sass/node-sass): Wrapper around libsass
- [raw-loader](https://github.com/webpack/raw-loader): raw loader module for webpack
- [sass-loader](https://github.com/jtangelder/sass-loader): Sass loader for webpack
- [style-loader](https://github.com/webpack/style-loader): style loader module for webpack
- [stylus](https://github.com/stylus/stylus): Robust, expressive, and feature-rich CSS superset
- [stylus-loader](https://github.com/shama/stylus-loader): Stylus loader for webpack
- [webpack-uglify-js-plugin](https://github.com/pingyuanChen/webpack-uglify-js-plugin): Incremental Uglify JS for webpack


## License

MIT
