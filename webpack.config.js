const path = require('path')
const webpack = require('webpack')
const poststylus = require('poststylus')
const BitBarWebpackProgressPlugin = require("bitbar-webpack-progress-plugin")

const NODE_ENV = process.env.NODE_ENV || 'development'


module.exports = {
    context: path.join(__dirname, '/src/js'),
    devtool: NODE_ENV == 'development' ? 'cheap-inline-module-source-map' : null,
    watch: true,

    watchOptions: {
        aggregateTimeout: 100
    },

    entry: {
        vending: './index.jsx',
        vendor: [
            'react',
            'redux',
            'react-redux',
            'semantic-ui-react'
        ]
    },

    output: {
        path: path.join(__dirname, '/dist/'),
        // Public Path for optimization require.ensure ;)
        publicPath: '/dist/',
        filename: '[name].js'
    },

    module: {
        loaders: [{
                test: [/\.js$/, /\.jsx$/],
                loaders: ['babel'],
                exclude: /\/node_modules\//,
                include: __dirname
            },
            {
                test: /\.css?$/,
                loader: 'style-loader!css-loader',
                include: __dirname
            },
            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.styl$/,
                loader: 'style-loader!css-loader!stylus-loader',
                exclude: /\/node_modules\//,
                include: __dirname
            },
            {
                test: /\.svg/,
                loader: 'svg-url-loader',
                exclude: /\/node_modules\//
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }
        ]
    },

    resolve: {
        root: path.resolve(__dirname, 'src', 'js'),
        modules: ['node_modules'],
        alias: {
            stylus: path.resolve(__dirname, 'src', 'styl'),
        },
        extensions: ['', '.js', '.jsx', '.styl']
    },

    plugins: [
        new webpack.NoErrorsPlugin(),

        new BitBarWebpackProgressPlugin(),

        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],

    stylus: {
        use: [
            poststylus(['autoprefixer'])
        ]
    }
}





if (NODE_ENV == 'production') {

    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            sourceMap: false,
            mangle: false,
            compress: {
                warnings: false
            }
        })
    );

    module.exports.watch = false;
};