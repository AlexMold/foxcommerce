import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import PageIndex from './containers/PageIndex';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import * as reducers from './reducers';


const reducer = combineReducers(reducers);
const store = createStore(reducer);


render(
    <Provider store={store}>
        <PageIndex />
    </Provider>,
    document.getElementById('root')
)