import React, {Component} from 'react';
import { Button, Form } from 'semantic-ui-react'


class AddCoins extends Component {
    constructor(props, context) {
        super(props, context)

        this.handleForm = this.handleForm.bind(this)
    }

    handleForm(e, { formData }) {
        const { addCoin } = this.props;
        console.log(' e ', e);
        console.log(' formData ', formData);
        e.preventDefault();

        addCoin(formData);
    }

    render() {
        return (
            <Form onSubmit={this.handleForm}>
                <Form.Field>
                    <label>Add coins</label>
                    <input 
                        name="nominal" 
                        type="number" 
                        max={2} 
                        min={0}
                        step={0.01}
                        placeholder="Max 2$" />
                </Form.Field>
                <Button type='submit' positive>Insert a coin</Button>
                {
                    !this.props.store.lucky_man ? 
                    <Button onClick={this.props.punchMachine} type='button' color="red">Kick the machine</Button> : 
                    false
                }
            </Form>
        );
    }
}

export default AddCoins;