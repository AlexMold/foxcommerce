import React, {Component} from 'react';
import { Grid } from 'semantic-ui-react'

class Body extends Component {
    render() {
        const { store } = this.props;

        return (
            <Grid columns={2}>
                <Grid.Row columns={2}>
                    <Grid.Column width={10}>Machine Answer: {store.status}</Grid.Column>
                    <Grid.Column width={6}>Money in Machine: {store.coins}$</Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default Body;