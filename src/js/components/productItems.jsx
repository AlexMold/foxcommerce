import React, {Component} from 'react';
import { Grid, Button } from 'semantic-ui-react'

class ProductItems extends Component {
    constructor(props, context) {
        super(props, context)

        this.mapItems = this.mapItems.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(el) {
        const { getProduct } = this.props;
        const productID = el.target.getAttribute('data-id');
        getProduct(productID);
    }

    mapItems() {
        const { products, coins, getProduct } = this.props;

        return products.map(item => 
            <Grid.Column key={item.id}>
                <Button 
                    type="button" 
                    data-id={item.id} 
                    positive={item.available}
                    onClick={this.handleClick}>
                    {item.title}
                </Button>
            </Grid.Column>
        )
    }

    render() {
        return (
            <Grid columns={4} divided>
                <Grid.Row centered>
                    {this.mapItems()}
                </Grid.Row>
            </Grid>
        )
    }
}

export default ProductItems;