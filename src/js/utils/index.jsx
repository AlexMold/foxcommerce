import * as types from "../constants/actionTypes";

export const isDestinyOnYourSide = (destiny, yourLucky) => yourLucky >= destiny ? true : false;

export const destiny = () => Math.round(Math.random()*10);

export const chooseMessage = (action) => {

    return (item) => {
        switch (action.type) {
            case types.ADD_COIN:
                const addedCoins = Number(action.nominal);
                return {
                    defaultMessage: `You add ${addedCoins}$!!!`
                }
            case types.GET_PRODUCT:
                return {
                    luckyMessage: `Are you got ${item.title}!!!`,
                    nonLucky: `For some reasons Vending Machine is broken.
                            Try later!!!`,
                    notEnoughMoney: `Not enough money for ${item.title}!!!`
                }
            case types.KICK_MACHINE:
                return {
                    luckyMessage: `Are you still got ${item.title}!!!`,
                    tryOneMore: 'Try one more time bro :)'
                }
        }
    }
}