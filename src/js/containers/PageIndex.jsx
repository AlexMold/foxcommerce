import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Container, Header } from 'semantic-ui-react'
import ProductItems from '../components/productItems'
import Body from '../components/body'
import AddCoins from '../components/addCoins'

import * as MachineActions from '../actions/index'

class PageIndex extends Component {
    render() {
        return (
            <Container text>
                <br/>
                <Header as="h1">Vending Machine</Header>
                <ProductItems {...this.props} />
                <br/>
                <br/>
                <Body {...this.props} />
                <br/>
                <br/>
                <AddCoins {...this.props} />
            </Container>
        )

    }
}

const mapStateToProps = (state, props) => ({
    products: state.default.vending.products,
    store: state.default.vending
});

const mapDispatchToProps = (dispatch, ownProps) => {
    console.log(' ownProps ', ownProps);

    return {
        addCoin: (nominal) => {
            dispatch(MachineActions.addCoin(nominal));
        },
        getProduct: (id) => {
            dispatch(MachineActions.getProduct(id));
        },
        punchMachine: () => {
            dispatch(MachineActions.punchMachine());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageIndex)