import * as types from '../constants/actionTypes';

export const addCoin = ({ nominal }) => ({ type: types.ADD_COIN, nominal });

export const getProduct = (id) => ({ type: types.GET_PRODUCT, id });

export const punchMachine = () => ({ type: types.KICK_MACHINE });

