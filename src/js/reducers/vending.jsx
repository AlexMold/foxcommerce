import * as types from "../constants/actionTypes";
import { isDestinyOnYourSide, destiny, chooseMessage } from "../utils/index"
import config from '../config'

const initialState = {
    coins: 0,
    products: [
        {
            id: 1,
            title: 'Coffee',
            price: 3.5,
            available: false
        },
        {
            id: 2,
            title: 'Chocolate',
            price: 1.75,
            available: false
        },
        {
            id: 3,
            title: 'Candies',
            price: 1,
            available: false
        },
        {
            id: 4,
            title: 'Gum',
            price: 0.75,
            available: false
        }
    ],
    needToReturn: [],
    status: 'Welcome!!!',
    lucky_man: true
}

const toggleStatus = (state, amountCoins) => {
    return state.products.map(item => {
        if (item.price <= amountCoins) {
            item.available = true
        } else {
            item.available = false
        }
        return item;
    });
}

export default function vending(state = initialState, action) {
    let amountCoins = null;
    let toggledProducts = [];
    const message = chooseMessage(action);

    switch (action.type) {

        case types.ADD_COIN:
            const addedCoins = Number(action.nominal);
            amountCoins = state.coins + addedCoins;
            toggledProducts = toggleStatus(state, state.coins + addedCoins);

            return {
                ...state,
                products: toggledProducts,
                coins: amountCoins,
                status: message().defaultMessage
            }


        case types.GET_PRODUCT:
            const productItem = state.products.filter(item => action.id == item.id)[0];
            const destinyOnYourSide = isDestinyOnYourSide(destiny(), config.my_luck);
            
            amountCoins = state.coins - productItem.price;
            toggledProducts = toggleStatus(state, amountCoins);


            if (productItem.price <= state.coins) {

                if (destinyOnYourSide && state.lucky_man) {
                    return {
                        ...state,
                        products: toggledProducts,
                        status: message(productItem).luckyMessage,
                        coins: amountCoins
                    }
                } else {
                    return {
                        ...state,
                        status: message(productItem).nonLucky,
                        needToReturn: [productItem],
                        lucky_man: false
                    }
                }

            } else {
                return {
                    ...state,
                    status: message(productItem).notEnoughMoney
                }
            }


        case types.KICK_MACHINE:
            const destinyOnMySide = isDestinyOnYourSide(destiny(), config.my_luck);
            const product = state.needToReturn[0];

            amountCoins = state.coins - product.price;

            if (destinyOnMySide) {
                return {
                    ...state,
                    lucky_man: destinyOnMySide,
                    needToReturn: state.needToReturn.pop(),
                    status: message(product).luckyMessage,
                    coins: amountCoins
                }
            } else {
                return {
                    ...state,
                    status: message(product).tryOneMore
                }
            }

        default:
            return state;
    }
}
